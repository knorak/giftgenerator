package giftGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

public class Kingitused {

	// Tee failideni .../src/kingitused_txt paketis
	public String failiTee = System.getProperty("user.dir") + java.io.File.separator + "src" + java.io.File.separator
			+ "kingitused_txt";

	/**
	 * Loob uue faili ja lisab kingid vastavalt kasutaja sisestusele
	 * 
	 * @param failiNimi
	 *            - Loodava faili nimi (ühtlasi inimese nimi)
	 * @param sisu
	 *            - faili sisu
	 */
	public void looFail(String failiNimi, String sisu) {

		try {
			PrintWriter trykkija = new PrintWriter(this.failiTee + File.separator + failiNimi + ".txt");
			String[] sisuArray = sisu.split(","); // Poolitab kingid, et neid
													// saaks panna eraldi
													// ridadele

			for (int i = 0; i < sisuArray.length; i++) {
				String kink = sisuArray[i].trim(); // eemaldab tühikud
				if (!kink.isEmpty()) // jätab tühjad read vahele
					trykkija.println(kink);
			}
			trykkija.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	/**
	 * loeb antud faili sisu (eeskujuks praktikumis 13 tehtud Faililugeja klass)
	 * 
	 * @param failitee
	 *            - Tee failini
	 * @return read - Read failist
	 */
	public ArrayList<String> loeFail(String failiTee) {
		File fail = new File(failiTee);

		ArrayList<String> read = new ArrayList<String>();

		try {
			// avab faili lugemiseks
			BufferedReader in = new BufferedReader(new FileReader(fail));
			String rida;

			// Lisab iga rea ArrayList-i
			while ((rida = in.readLine()) != null) {
				read.add(rida);
			}

			in.close();

		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		return read;
	}

	/**
	 * tagastab HTML-i, et kuvada igale inimesele suvalisi kingitusi
	 * 
	 * @param inimesenimi
	 *            - inimese nimi, kellele suvaline kink genereeritakse
	 * 
	 * @return html - HTML string
	 */
	public String KingiHtml(String inimesenimi) {
		String html = "<html>";

		// võtab kinkide nimekirja
		ArrayList<String> kingid = loeFail(failiTee + File.separator + inimesenimi + ".txt");
		Random r = new Random();
		String suvalineKink = kingid.get(r.nextInt(kingid.size())); // võtab
																	// suvalise
																	// kingi
																	// nimekirjast

		html += inimesenimi + " - " + suvalineKink + "<br/>"; // Loob HTML
																// Stringi
																// inimestest ja
																// nende
																// kinkidest
																// (<br> silt on
																// ridade
																// murdmiseks)

		html += "</html>";

		return html;
	}

	/**
	 * Loeb kokku, mitu inimest on kinginimekirjas
	 * 
	 * @return loenda - tagastab inimeste arvu
	 */
	public int mituInimest() {
		int loenda = 0;

		File[] failid = new File(failiTee).listFiles();

		for (File fail : failid) {
			if (fail.isFile()) {
				loenda++;
			}
		}

		return loenda;
	}

	/**
	 * Loeb mis inimestele on loodud kingifailid.
	 * 
	 * @return inimesed2 - tagastab ArrayListi inimeste nimedest
	 */
	public ArrayList<String> loeInimesed() {

		ArrayList<String> inimesed2 = new ArrayList<String>();
		File[] failid = new File(failiTee).listFiles();
		for (File fail : failid) {
			if (fail.isFile()) {
				inimesed2.add(fail.getName().replace(".txt", ""));
			}
		}
		return inimesed2;
	}

	/**
	 * Kustutab inimese tekstifaili
	 * 
	 * @param inimesenimi
	 *            - inimene, kelle fail kustutatakse
	 */
	public void kustutaInimene(String inimesenimi) {
		File f = new File(failiTee + File.separator + inimesenimi + ".txt");
		f.delete();
	}

	/**
	 * Võtab tekstifailist kingituste nimekirja
	 * 
	 * @param inimesenimi
	 *            - inimene, kelle kingitusi soovitakse näha
	 * @return kingitusedString - tagastab kingitused stringina (eraldatud
	 *         komaga)
	 */
	public String kingitusteTekst(String inimesenimi) {

		// võtab kinkide nimekirja
		ArrayList<String> kingitused = loeFail(failiTee + File.separator + inimesenimi + ".txt");
		String kingitusedString = "";
		for (String s : kingitused) {
			kingitusedString += s + ",";
		}
		return kingitusedString;
	}

}
