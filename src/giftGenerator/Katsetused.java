package giftGenerator;

import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class Katsetused extends JFrame {

	private static String valitudNimi = new String();
	private static DefaultListModel<String> model = new DefaultListModel<String>();
	private static int valitudindex;

	/**
	 * Kujundus (raamid, paneelid, nupud jms)
	 */
	public static void displayFrame() {

		JFrame raam = new JFrame("Gift generator"); // pealkiri
		raam.setVisible(true); // Teeb akna nähtavaks
		raam.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Sulgeb akna, kui
															// vajutatakse
															// "X"-i
		raam.setSize(320, 150); // Akna suurus

		JPanel container = new JPanel();
		JPanel paneel1 = new JPanel();
		JPanel paneel2 = new JPanel();

		container.add(paneel1);
		container.add(paneel2);
		raam.add(container);
		paneel1.setLayout(new BoxLayout(paneel1, BoxLayout.PAGE_AXIS));

		// Nimekiri olemasolevatest inimeste nimedest
		ArrayList<String> nimed = new ArrayList<String>();
		nimed = new Kingitused().loeInimesed();
		for (String p : nimed) {
			model.addElement(p);
		}
		JList list;
		list = new JList(model);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent ev) {
				if (list.getSelectedValue() != null) {
					valitudNimi = list.getSelectedValue().toString();
					valitudindex = list.getSelectedIndex();
				}

			}
		});

		// Nupp "Genereeri kingitusi"
		JButton giftGeneratorNupp = new JButton("Genereeri kingitusi");
		paneel1.add(giftGeneratorNupp);

		paneel2.add(list);
		// Event handler "Genereeri kingitusi"-le
		giftGeneratorNupp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (list.getSelectedValue() != null) {

					Integer inimesteArv = new Kingitused().mituInimest();

					JFrame frame2 = new JFrame("Kinkide nimekiri");
					frame2.setVisible(true);
					frame2.setSize(350, inimesteArv * 35); // Akna suurus sõltub
					// inimeste arvust
					// nimekirjas
					JLabel silt = new JLabel();
					String kingid = new Katsetused().saaKingid(valitudNimi.toString());
					silt.setText(kingid);
					JPanel paneel = new JPanel();
					frame2.add(paneel);
					paneel.add(silt);
				} else {
					JOptionPane.showMessageDialog(null, "Inimese nimi on valimata");
				}
			}

		});

		// Nupp "Kustuta inimene"
		JButton giftDeleteNupp = new JButton("Kustuta inimene");
		paneel1.add(giftDeleteNupp);

		// Event handler "Kustuta inimene"-le
		giftDeleteNupp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Kingitused().kustutaInimene(valitudNimi);
				model.removeElementAt(valitudindex);
			}
		});

		// Nupp "Uus inimene"
		JButton uusInimeneNupp = new JButton("Uus inimene");
		paneel1.add(uusInimeneNupp);

		// Event handler "Uus inimene"-le.
		uusInimeneNupp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nimi = JOptionPane.showInputDialog("Uue inimese nimi");

				if (nimi == null || nimi.isEmpty()) // JOptionPane'il
													// canceli vajutamine on
													// võrdne
													// "null"-iga
					return;

				String kingid = JOptionPane.showInputDialog("Sisesta kingitused uuele inimesele (eralda komaga)");

				if (!kingid.isEmpty()) {
					new Kingitused().looFail(nimi, kingid);
					model.addElement(nimi);
				}

			}

		});

		// Nupp "Muuda kingitusi"
		JButton muudaNupp = new JButton("Muuda kingitusi");
		paneel1.add(muudaNupp);

		// Event handler "Muuda kingitusi"-le
		muudaNupp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (list.getSelectedValue() != null) {

					String kingitused = new Kingitused().kingitusteTekst(valitudNimi);

					String in = JOptionPane.showInputDialog(null, "Tee soovitud muudatused", kingitused);

					if (in == null || in.isEmpty())
						return;

				} else {
					JOptionPane.showMessageDialog(null, "Inimese nimi on valimata");
				}
			}
		});

		raam.revalidate();
	}

	public static void main(String[] args) {
		displayFrame();

	};

	/**
	 * Kingid igale inimesele
	 * 
	 * @return kingid - Html string
	 */

	public String saaKingid(String nimi) {
		Kingitused failiHaldur = new Kingitused();
		String kingid = failiHaldur.KingiHtml(nimi);

		return kingid;
	}

	/**
	 * Uue inimese lisamine
	 * 
	 * @param nimi
	 *            uue inimese nimi
	 * @param kingid
	 *            - Kingitused (komaga eraldatud väärtused)
	 */
	public void uusInimene(String nimi, String kingid) {
		Kingitused failiHaldur = new Kingitused();
		failiHaldur.looFail(nimi, kingid);
	}

}
